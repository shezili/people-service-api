﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using dotnet_api_ps.Models;
using dotnet_api_ps.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace dotnet_api_ps.Controllers {

    [Route ("api/[controller]")]
    [ApiController]
    public class PeopleController : ControllerBase {

        // GET api/values
        [HttpGet]

        public ActionResult<IEnumerable<string>> Get () {
            List<People> peopleList = PeopleService.GetAll ();
            SearchResult searchResult = new SearchResult ();
            searchResult.Results = peopleList;
            searchResult.TotalCount = peopleList.Count;

            return Ok (searchResult);
        }

        // GET api/values/5
        [HttpGet ("{id}")]
        public ActionResult<string> Get (string id) {
            var people = PeopleService.GetPeopleById (id);
            if (people != null) {
                return Ok (people);
            }
            return NotFound ();
        }

        // POST api/values
        [HttpPost]
        public ActionResult<IEnumerable<string>> Post ([FromBody] People value) {
            if (string.IsNullOrEmpty (value.Gender)) {
                return BadRequest ("Gender can not be empty.");
            }
            if (value.Gender.ToLower () != "male" && value.Gender.ToLower () != "female" && value.Gender.ToLower () != "Not sure") {
                return BadRequest ("Gender is invalid."); //wrong value
            }
            if (!IsNameValid (value.Name)) {
                return BadRequest ("Title, First name and Last name can not be empty.");
            }

            if (!IsValidEmail (value.Email)) {
                return BadRequest ("Email is invalid.");
            }
            //IsValidDob (value.Dob);
            if (value.Dob > DateTime.Now) {
                return BadRequest ("Dob can not be the future date.");
            }
            if (value.Registered > DateTime.Now) {
                return BadRequest ("Registered can not be the future date.");
            }
            if (!IsUriValid (value.Picture)) {
                return BadRequest ("Picture is invalid");
            }
            if (IsExistingUser (value.Email)) {
                return BadRequest ("User email is already in our database, cannot add again.");
            }
            var id = PeopleService.Add (value);
            return Ok (new { id = id });
        }

        private bool IsExistingUser (string email) {
            if (PeopleService.GetPeopleByEmail (email) == null) {
                return false;
            }
            return true;
        }
        // PUT api/values/5
        [HttpPut ("{id}")]
        public ActionResult<IEnumerable<string>> Put (string id, [FromBody] People value) {
            if (string.IsNullOrEmpty (value.Gender)) {
                return BadRequest ("Gender can not be empty.");
            }
            if (value.Gender.ToLower () != "male" && value.Gender.ToLower () != "female" && value.Gender.ToLower () != "Not sure") {
                return BadRequest ("Gender is invalid."); //wrong value
            }
            if (!IsNameValid (value.Name)) {
                return BadRequest ("Title, First name and Last name can not be empty.");
            }

            if (!IsValidEmail (value.Email)) {
                return BadRequest ("Email is invalid.");
            }
            //IsValidDob (value.Dob);
            if (value.Dob > DateTime.Now) {
                return BadRequest ("Dob can not be the future date.");
            }
            if (value.Registered > DateTime.Now) {
                return BadRequest ("Registered can not be the future date.");
            }
            if (!IsUriValid (value.Picture)) {
                return BadRequest ("Picture is invalid");
            }
            PeopleService.Update (id, value);
            return Ok ();
        }

        // DELETE api/people/5
        [HttpDelete ("{id}")]
        public void Delete (string id) {
            PeopleService.Delete (id);
        }

        public bool IsValidEmail (string email) {
            string MatchEmailPattern =
                @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@" +
                @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\." +
                @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|" +
                @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";
            Console.WriteLine (email);
            if (email != null) {
                return Regex.IsMatch (email, MatchEmailPattern);
            }
            return false;
        }

        public static bool IsUriValid (string uri) {
            Uri uriResult;
            bool result = Uri.TryCreate (uri, UriKind.Absolute, out uriResult) &&
                (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
            return result;
        }

        private bool IsNameValid (Name name) {
            if (name == null) {
                return false;
            }
            if (string.IsNullOrEmpty (name.Title)) {
                return false;
            }
            if (string.IsNullOrEmpty (name.First)) {
                return false;
            }
            if (string.IsNullOrEmpty (name.Last)) {
                return false;
            }
            return true;
        }

    }
}