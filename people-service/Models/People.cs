using System;
using System.Runtime.Serialization;

namespace dotnet_api_ps.Models {

    public class People {

        public string Gender { get; set; }
        public Name Name { get; set; }
        // List<Name> personName = new List<Name> ();
        // personName.Add(new Name {
        //     Title = "mr",
        //         First = "daniel",
        //         Last = "cunningham"
        // });
        public string Email { get; set; }
        public DateTime? Dob { get; set; }
        public DateTime? Registered { get; set; }
        public string Phone { get; set; }
        public string Cell { get; set; }
        public string Id { get; set; }
        [DataMember]
        public string Picture { get; set; }
    }
}

//gender","name":{"title":"mr","first":"daniel","last":"cunningham"},
//"email":"daniel.cunningham@example.com","dob":"1948-06-17 19:07:40","
//registered":"2013-11-21 22:10:42","phone":"06-2244-3501","cell":"0484-010-369","id":"590115570","picture"

// Name = name, Age = age

//string gender, string email, DateTime dob, DateTime registered, string phone, string cell, string id, string picture
// // if (string.IsNullOrEmpty (gender)) {
//                 return BadRequest ("Name is required.");
//             }

//             if (age < 1) {
//                 return BadRequest ("Age must be greater than or equal to 1.");
//             }