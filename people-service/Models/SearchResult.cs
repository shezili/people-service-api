using System.Collections.Generic;

namespace dotnet_api_ps.Models {
    public class SearchResult {
        public List<People> Results { get; set; }
        public int TotalCount { get; set; }
    }
}