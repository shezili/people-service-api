using System.Runtime.Serialization;

namespace dotnet_api_ps.Models {
    [DataContract]
    public class Name {
        [DataMember]

        public string Title { get; set; }
        [DataMember]

        public string First { get; set; }
        [DataMember]

        public string Last { get; set; }
    }
}