using System;
using System.Collections.Generic;
using System.Linq;
using dotnet_api_ps.Models;

namespace dotnet_api_ps.Services {
    public static class PeopleService {
        static PeopleService () {
            NextId = 8;
            PeopleList = new List<People> ();
            PeopleList.Add (
                new People {
                    Gender = "male",
                        Name = new Name {
                            Title = "mr",
                                First = "daniel",
                                Last = "cunningham"
                        },
                        Email = "daniel.cunningham@example.com",
                        Dob = new DateTime (1948, 6, 17, 19, 7, 40),
                        Registered = new DateTime (2013, 11, 21, 22, 10, 42),
                        Phone = "0400000002",
                        Cell = "0400000003",
                        Id = "1",
                        Picture = "https://randomuser.me/api/portraits/men/60.jpg"
                });
            PeopleList.Add (
                new People {
                    Gender = "female",
                        Name = new Name {
                            Title = "ms",
                                First = "jane",
                                Last = "doe"
                        },
                        Email = "jane.do@example.com",
                        Dob = new DateTime (1948, 6, 17, 19, 7, 40),
                        Registered = new DateTime (2013, 11, 21, 22, 10, 42),
                        Phone = "0400000001",
                        Cell = "0400000001",
                        Id = "2",
                        Picture = "https://randomuser.me/api/portraits/women/63.jpg"
                });
            PeopleList.Add (
                new People {
                    Gender = "female",

                        Name = new Name {
                            Title = "miss",
                                First = "patsy",
                                Last = "matthews"
                        },
                        Email = "patsy.matthews@example.com",
                        Dob = new DateTime (1948, 6, 17, 19, 7, 40),
                        Registered = new DateTime (2013, 11, 21, 22, 10, 42),
                        Phone = "0400000002",
                        Cell = "0400000003",
                        Id = "3",
                        Picture = "https://randomuser.me/api/portraits/women/60.jpg"
                });
            PeopleList.Add (
                new People {
                    Gender = "male",

                        Name = new Name {
                            Title = "mr",
                                First = "byron",
                                Last = "snyder"
                        },
                        Email = "byron.snyder@example.com",
                        Dob = new DateTime (1948, 6, 17, 19, 7, 40),
                        Registered = new DateTime (2013, 11, 21, 22, 10, 42),
                        Phone = "0400000002",
                        Cell = "0400000003",
                        Id = "4",
                        Picture = "https://randomuser.me/api/portraits/men/68.jpg"
                });
            PeopleList.Add (
                new People {
                    Gender = "male",

                        Name = new Name {
                            Title = "mr",
                                First = "alan",
                                Last = "mccoy"
                        },
                        Email = "alan.mccoy@example.com",
                        Dob = new DateTime (1948, 6, 17, 19, 7, 40),
                        Registered = new DateTime (2013, 11, 21, 22, 10, 42),
                        Phone = "0400000002",
                        Cell = "0400000003",
                        Id = "5",
                        Picture = "https://randomuser.me/api/portraits/men/65.jpg"
                });
            PeopleList.Add (
                new People {
                    Gender = "female",

                        Name = new Name {
                            Title = "ms",
                                First = "joann",
                                Last = "allen"
                        },
                        Email = "joann.allen@example.com",
                        Dob = new DateTime (1948, 6, 17, 19, 7, 40),
                        Registered = new DateTime (2013, 11, 21, 22, 10, 42),
                        Phone = "0400000002",
                        Cell = "0400000003",
                        Id = "6",
                        Picture = "https://randomuser.me/api/portraits/women/44.jpg"
                });
            PeopleList.Add (
                new People {
                    Gender = "female",
                        Name = new Name {
                            Title = "ms",
                                First = "test",
                                Last = "user"
                        },
                        Email = "test.user@example.com",
                        Dob = new DateTime (1948, 6, 17, 19, 7, 40),
                        Registered = new DateTime (2013, 11, 21, 22, 10, 42),
                        Phone = "0400000002",
                        Cell = "0400000003",
                        Id = "7",
                        Picture = "https://randomuser.me/api/portraits/women/48.jpg"
                });
        }
        private static int NextId { get; set; }
        public static List<People> PeopleList { get; set; }
        public static List<People> GetAll () {
            return PeopleList;
        }
        public static string Add (People people) {
            people.Registered = DateTime.Now;
            people.Id = NextId.ToString ();
            PeopleList.Add (people);
            return NextId.ToString ();
            NextId++;
        }
        public static void Update (string id, People people) {
            var peopleData = PeopleList.Where (p => p.Id == id).FirstOrDefault ();

            if (peopleData != null) {

                peopleData.Name.Title = people.Name.Title;
                peopleData.Name.First = people.Name.First;
                peopleData.Name.Last = people.Name.Last;
                peopleData.Cell = people.Cell;
                peopleData.Dob = people.Dob;
                peopleData.Gender = people.Gender;
                peopleData.Phone = people.Phone;
                peopleData.Picture = people.Picture;
                peopleData.Email = people.Email;
            }
        }
        public static void Delete (string id) {
            var peopleData = PeopleList.Where (p => p.Id == id).FirstOrDefault ();
            if (peopleData != null) {
                PeopleList.Remove (peopleData);
            }
        }
        public static People GetPeopleByEmail (string email) {
            return PeopleList.Where (p => p.Email == email).FirstOrDefault ();
        }
        public static People GetPeopleById (string id) {
            return PeopleList.Where (p => p.Email == id).FirstOrDefault ();
        }
    }
}