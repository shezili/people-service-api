using System;
using System.Collections.Generic;
using System.Linq;
using dotnet_api_ps.Models;
using dotnet_api_ps.Services;
using Xunit;

namespace people_service_tests {
    public class PeopleServiceTests {
        [Fact]
        public void AddNewPeople_WhenAddedCorrectly_PeopleShouldBeAddedToPeopleList () {
            var testPeopleList = new List<People> ();
            testPeopleList.Add (new People { Id = "1" });
            PeopleService.PeopleList = testPeopleList;
            var id = PeopleService.Add (new People { Id = "2" });

            Assert.Equal (2, PeopleService.PeopleList.Count);
            Assert.Equal (1, PeopleService.PeopleList.Where (p => p.Id == id).Count ());
        }

        [Fact]
        public void UpdatePeople_WhenUpdatedCorrectly_PeopleListShouldBeUpdated () {
            var testPeopleList = new List<People> ();
            var name = new Name () { Title = "ms", First = "Tina", Last = "Test" };

            testPeopleList.Add (new People {
                Id = "1",
                    Name = name,
                    Email = "abc@abc.com",
                    Cell = "12",
                    Dob = DateTime.Now,
                    Gender = "female",
                    Phone = "123",
                    Picture = "http://test"
            });
            PeopleService.PeopleList = testPeopleList;
            PeopleService.Update ("1", new People {
                Id = "1",
                    Name = name,
                    Email = "def@def.com",
                    Cell = "12",
                    Dob = DateTime.Now,
                    Gender = "female",
                    Phone = "123",
                    Picture = "http://test"
            });

            Assert.Equal (1, PeopleService.PeopleList.Count);
            Assert.Equal ("def@def.com", PeopleService.PeopleList[0].Email);
        }

        [Fact]
        public void DeletePeople_WhenDeletedCorrectly_PeopleShouldBeRemovedFromPeopleList () {
            var testPeopleList = new List<People> ();
            testPeopleList.Add (new People { Id = "1" });
            testPeopleList.Add (new People { Id = "2" });

            PeopleService.PeopleList = testPeopleList;

            PeopleService.Delete ("1");

            Assert.Equal (1, PeopleService.PeopleList.Count);
            Assert.Equal (0, PeopleService.PeopleList.Where (p => p.Id == "1").Count ());
            Assert.Equal (1, PeopleService.PeopleList.Where (p => p.Id == "2").Count ());
        }
    }
}